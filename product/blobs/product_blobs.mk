#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions.xml

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/etc/permissions/com.android.omadm.service.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.omadm.service.xml \
    vendor/gapps/product/blobs/etc/permissions/com.android.sdm.plugins.connmo.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.sdm.plugins.connmo.xml \
    vendor/gapps/product/blobs/etc/permissions/com.android.sdm.plugins.dcmo.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.sdm.plugins.dcmo.xml \
    vendor/gapps/product/blobs/etc/permissions/com.android.sdm.plugins.diagmon.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.sdm.plugins.diagmon.xml \
    vendor/gapps/product/blobs/etc/permissions/com.android.sdm.plugins.sprintdm.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.sdm.plugins.sprintdm.xml \
    vendor/gapps/product/blobs/etc/permissions/com.android.sdm.plugins.usccdm.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.sdm.plugins.usccdm.xml \
    vendor/gapps/product/blobs/etc/permissions/com.customermobile.preload.vzw.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.customermobile.preload.vzw.xml \
    vendor/gapps/product/blobs/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/gapps/product/blobs/etc/permissions/com.google.omadm.trigger.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.omadm.trigger.xml \
    vendor/gapps/product/blobs/etc/permissions/com.verizon.apn.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.verizon.apn.xml \
    vendor/gapps/product/blobs/etc/permissions/com.verizon.services.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.verizon.services.xml \
    vendor/gapps/product/blobs/etc/permissions/features-verizon.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/features-verizon.xml \
    vendor/gapps/product/blobs/etc/permissions/obdm_permissions.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/obdm_permissions.xml \
    vendor/gapps/product/blobs/etc/permissions/privapp-permissions-google-p.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google-p.xml \
    vendor/gapps/product/blobs/etc/permissions/split-permissions-google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/split-permissions-google.xml \
    vendor/gapps/product/blobs/etc/permissions/vzw_mvs_permissions.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/vzw_mvs_permissions.xml

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/preferred-apps/google.xml

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/etc/sysconfig/allowlist_com.android.omadm.service.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/allowlist_com.android.omadm.service.xml \
    vendor/gapps/product/blobs/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/gapps/product/blobs/etc/sysconfig/google-rollback-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-rollback-package-whitelist.xml \
    vendor/gapps/product/blobs/etc/sysconfig/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-staged-installer-whitelist.xml \
    vendor/gapps/product/blobs/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google.xml \
    vendor/gapps/product/blobs/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google_build.xml \
    vendor/gapps/product/blobs/etc/sysconfig/nexus.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nexus.xml \
    vendor/gapps/product/blobs/etc/sysconfig/nga.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nga.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2017.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2017.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2018.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2018.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2019.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2019.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2019_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2019_midyear.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2020_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2020_midyear.xml \
    vendor/gapps/product/blobs/etc/sysconfig/pixel_experience_2020.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2020.xml \
    vendor/gapps/product/blobs/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/lib64/libakuaf.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libakuaf.so \
    vendor/gapps/product/blobs/lib64/libborders_scone_leveldb_jni.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libborders_scone_leveldb_jni.so \
    vendor/gapps/product/blobs/lib64/libgdx.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libgdx.so \
    vendor/gapps/product/blobs/lib64/libmotricity.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libmotricity.so \
    vendor/gapps/product/blobs/lib64/libsketchology_native.so:$(TARGET_COPY_OUT_PRODUCT)/lib64/libsketchology_native.so

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/lib/libdmengine.so:$(TARGET_COPY_OUT_PRODUCT)/lib/libdmengine.so \
    vendor/gapps/product/blobs/lib/libdmjavaplugin.so:$(TARGET_COPY_OUT_PRODUCT)/lib/libdmjavaplugin.so

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/usr/share/ime/google/d3_lms/ko_2018030706.zip:$(TARGET_COPY_OUT_PRODUCT)/usr/share/ime/google/d3_lms/ko_2018030706.zip \
    vendor/gapps/product/blobs/usr/share/ime/google/d3_lms/mozc.data:$(TARGET_COPY_OUT_PRODUCT)/usr/share/ime/google/d3_lms/mozc.data \
    vendor/gapps/product/blobs/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip:$(TARGET_COPY_OUT_PRODUCT)/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip

PRODUCT_COPY_FILES += \
    vendor/gapps/product/blobs/usr/srec/en-US/am_phonemes.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/am_phonemes.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/APP_NAME.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/APP_NAME.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/APP_NAME.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/APP_NAME.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/c_fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/c_fst \
    vendor/gapps/product/blobs/usr/srec/en-US/CLG.prewalk.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/CLG.prewalk.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/commands.abnf:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/commands.abnf \
    vendor/gapps/product/blobs/usr/srec/en-US/compile_grammar.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/compile_grammar.config \
    vendor/gapps/product/blobs/usr/srec/en-US/config.pumpkin:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/config.pumpkin \
    vendor/gapps/product/blobs/usr/srec/en-US/CONTACT_NAME.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/CONTACT_NAME.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/CONTACT_NAME.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/CONTACT_NAME.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/contacts.abnf:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/contacts.abnf \
    vendor/gapps/product/blobs/usr/srec/en-US/CONTACT.transform.mfar:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/CONTACT.transform.mfar \
    vendor/gapps/product/blobs/usr/srec/en-US/dict:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/dict \
    vendor/gapps/product/blobs/usr/srec/en-US/dictation.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/dictation.config \
    vendor/gapps/product/blobs/usr/srec/en-US/dnn:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/dnn \
    vendor/gapps/product/blobs/usr/srec/en-US/embedded_class_denorm.mfar:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/embedded_class_denorm.mfar \
    vendor/gapps/product/blobs/usr/srec/en-US/embedded_normalizer.mfar:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/embedded_normalizer.mfar \
    vendor/gapps/product/blobs/usr/srec/en-US/endpointer_dictation.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/endpointer_dictation.config \
    vendor/gapps/product/blobs/usr/srec/en-US/endpointer_model:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/endpointer_model \
    vendor/gapps/product/blobs/usr/srec/en-US/endpointer_model.mmap:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/endpointer_model.mmap \
    vendor/gapps/product/blobs/usr/srec/en-US/endpointer_voicesearch.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/endpointer_voicesearch.config \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_app-actions_prompted-app-name_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_app-actions_prompted-app-name_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_calendar-actions_AddCalendarEvent-Prompted-FieldToChange_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_calendar-actions_AddCalendarEvent-Prompted-FieldToChange_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_confirmation_confirmation-cancellation_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_confirmation_confirmation-cancellation_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_gmm-actions_gmm-nav-actions_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_gmm-actions_gmm-nav-actions_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_media-actions_music-service-controllable_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_media-actions_music-service-controllable_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_monastery_contact-disambig-static_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_monastery_contact-disambig-static_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_monastery_GenericAction-Prompted-ContactName_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_monastery_GenericAction-Prompted-ContactName_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Read_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Read_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Skip_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Skip_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/en-US_time-actions_time-context_TWIDDLER_FST.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/en-US_time-actions_time-context_TWIDDLER_FST.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/ep_portable_mean_stddev:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/ep_portable_mean_stddev \
    vendor/gapps/product/blobs/usr/srec/en-US/ep_portable_model.uint8.mmap:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/ep_portable_model.uint8.mmap \
    vendor/gapps/product/blobs/usr/srec/en-US/g2p.data:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/g2p.data \
    vendor/gapps/product/blobs/usr/srec/en-US/g2p_fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/g2p_fst \
    vendor/gapps/product/blobs/usr/srec/en-US/g2p_graphemes.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/g2p_graphemes.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/g2p_phonemes.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/g2p_phonemes.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/grammar.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/grammar.config \
    vendor/gapps/product/blobs/usr/srec/en-US/hmmlist:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/hmmlist \
    vendor/gapps/product/blobs/usr/srec/en-US/hmm_symbols:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/hmm_symbols \
    vendor/gapps/product/blobs/usr/srec/en-US/input_mean_std_dev:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/input_mean_std_dev \
    vendor/gapps/product/blobs/usr/srec/en-US/lexicon.U.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/lexicon.U.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/lstm_model.uint8.data:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/lstm_model.uint8.data \
    vendor/gapps/product/blobs/usr/srec/en-US/magic_mic.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/magic_mic.config \
    vendor/gapps/product/blobs/usr/srec/en-US/metadata:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/metadata \
    vendor/gapps/product/blobs/usr/srec/en-US/monastery_config.pumpkin:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/monastery_config.pumpkin \
    vendor/gapps/product/blobs/usr/srec/en-US/norm_fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/norm_fst \
    vendor/gapps/product/blobs/usr/srec/en-US/offensive_word_normalizer.mfar:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/offensive_word_normalizer.mfar \
    vendor/gapps/product/blobs/usr/srec/en-US/offline_action_data.pb:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/offline_action_data.pb \
    vendor/gapps/product/blobs/usr/srec/en-US/phonelist:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/phonelist \
    vendor/gapps/product/blobs/usr/srec/en-US/portable_lstm:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/portable_lstm \
    vendor/gapps/product/blobs/usr/srec/en-US/portable_meanstddev:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/portable_meanstddev \
    vendor/gapps/product/blobs/usr/srec/en-US/prons_exception_dictionary_file.txt:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/prons_exception_dictionary_file.txt \
    vendor/gapps/product/blobs/usr/srec/en-US/pumpkin.mmap:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/pumpkin.mmap \
    vendor/gapps/product/blobs/usr/srec/en-US/rescoring.fst.compact:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/rescoring.fst.compact \
    vendor/gapps/product/blobs/usr/srec/en-US/semantics.pumpkin:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/semantics.pumpkin \
    vendor/gapps/product/blobs/usr/srec/en-US/SONG_NAME.fst:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/SONG_NAME.fst \
    vendor/gapps/product/blobs/usr/srec/en-US/SONG_NAME.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/SONG_NAME.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.main_model.uint8.data:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.main_model.uint8.data \
    vendor/gapps/product/blobs/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.self_normalized_model.uint8.data:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.self_normalized_model.uint8.data \
    vendor/gapps/product/blobs/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.syms \
    vendor/gapps/product/blobs/usr/srec/en-US/verbalizer_terse.mfar:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/verbalizer_terse.mfar \
    vendor/gapps/product/blobs/usr/srec/en-US/voice_actions_compiler.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/voice_actions_compiler.config \
    vendor/gapps/product/blobs/usr/srec/en-US/voice_actions.config:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/voice_actions.config \
    vendor/gapps/product/blobs/usr/srec/en-US/word_confidence_classifier:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/word_confidence_classifier \
    vendor/gapps/product/blobs/usr/srec/en-US/wordlist.syms:$(TARGET_COPY_OUT_PRODUCT)/usr/srec/en-US/wordlist.syms
